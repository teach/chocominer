import logging
import os
import requests
import threading
import time
from influxdb import InfluxDBClient

DEBUG = os.environ.get("DEBUG", "").lower().startswith("y")
INFLUXDB_HOST = os.environ.get("INFLUXDB_HOST", "influxdb")
RNG_HOST = os.environ.get("RNG_HOST", "rng")
HASHER_HOST = os.environ.get("HASHER_HOST", "hasher")

log = logging.getLogger(__name__)
if DEBUG:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("requests").setLevel(logging.WARNING)

client = InfluxDBClient(INFLUXDB_HOST, 8086, os.environ.get("INFLUXDB_USER", "chocominer"), os.environ.get("INFLUXDB_USER_PASSWORD", "chocominer"), os.environ.get("INFLUXDB_DB", "chocominer"))

def get_random_bytes():
    r = requests.get("http://" + RNG_HOST + "/32")
    return r.content


def hash_bytes(data):
    r = requests.post("http://" + HASHER_HOST + "/",
                      data=data,
                      headers={"Content-Type": "application/octet-stream"})
    hex_hash = r.text
    return hex_hash

def current_chunk():
    r = requests.get("https://chunk.virli.nemunai.re/chunk")
    return r.content.decode()

def claim_chunk(random_bytes):
    r = requests.post("https://chunk.virli.nemunai.re/chunk",
                      data='{"proof": "' + random_bytes + '", "login": "' + os.environ.get("USER", "nemunaire") + '"}',
                      headers={"Content-Type": "application/json"})
    return r.content


chunk = "12"
def update_chunk():
    global chunk
    while True:
        try:
            chunk = current_chunk()
            time.sleep(1)
        except:
            pass

def work_loop(interval=1):
    deadline = 0
    loops_done = 0
    while True:
        if time.time() > deadline:
            log.info("{} units of work done, updating hash counter"
                     .format(loops_done))
            client.write_points([{
                "measurement": "hashes",
                "fields": {
                    "value": loops_done
                }
            }])
            loops_done = 0
            deadline = time.time() + interval
        work_once()
        loops_done += 1


def work_once():
    log.debug("Doing one unit of work")
    time.sleep(0.1)
    random_bytes = get_random_bytes()
    hex_hash = hash_bytes(random_bytes + chunk[0].encode())
    log.info("Nugget found: {}...{} (looking for {})".format(hex_hash[:8], hex_hash[-2:], chunk[1:]))
    client.write_points([{
        "measurement": "nuggets",
        "fields": {
            "value": str(hex_hash),
            "proof": random_bytes.hex(),
        }
    }])

    if hex_hash.startswith(chunk[1:]) and claim_chunk(random_bytes.hex()) == b"true":
        log.debug("CHUNK FOUND \o/")
        client.write_points([{
            "measurement": "chunks",
            "fields": {
                "value": str(hex_hash),
            }
        }])


if __name__ == "__main__":
    threading.Thread(target=update_chunk).start()
    while True:
        try:
            work_loop()
        except:
            log.exception("In work loop:")
            log.error("Waiting 10s and restarting.")
            time.sleep(10)
